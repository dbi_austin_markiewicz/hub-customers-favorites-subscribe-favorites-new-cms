const path = require('path');
const TerserPlugin = require('terser-webpack-plugin')

module.exports = {
    entry: {
        'hub-customers-favorites-subscribe-favorites-new-cms': './lib/hub-customers-favorites-subscribe-favorites-new-cms.ts'
    },
    module: {
        rules: [
          {
            test: /\.tsx?$/,
            use: 'ts-loader',
            exclude: /node_modules/
          }
        ]
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js' ]
    },

    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'packed'),
        libraryTarget: 'commonjs2'
    },
    externals: [
        'aws-sdk',
    ],
    node: {
         __dirname: false
    },
    optimization: {
        // usedExports: true,
        minimizer: [
            new TerserPlugin({
                terserOptions: {
                    extractComments: true,
                    keep_fnames: true,
                },
            })
        ]
    },
    target: 'node',
    // mode: 'development'
};
