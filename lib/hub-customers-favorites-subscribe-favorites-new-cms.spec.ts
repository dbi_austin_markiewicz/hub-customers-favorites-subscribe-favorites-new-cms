
/* tslint:disable: no-duplicate-imports */
import { expect } from 'chai';
import { Context, APIGatewayProxyEvent } from 'aws-lambda';
import { createSandbox } from 'sinon';

import { Logger, LambdaInstance } from '@dbidss/dss-apis-logger';
import { initLogger, DssResponseCode } from '@dbidss/dss-apis-datastore';
import { InvalidMethod, DssError } from '@dbidss/dss-apis-validation';
import * as service from './hub-customers-favorites-subscribe-favorites-new-cms';

describe('service', () => {
    const emptyLogger = initLogger({ headers: { loggingcontext: JSON.stringify({ library: 'empty' }) } } as any, {} as Context, {} as LambdaInstance);
    let logger: Logger;
    const sandbox = createSandbox();
    beforeEach(async () => {
        logger = await emptyLogger;
    });
    afterEach(() => {
        sandbox.restore();
    });

    describe('handle', () => {
        const emptyLogger = initLogger({} as APIGatewayProxyEvent, {} as Context, {} as LambdaInstance);
        let logger: Logger;
        const sandbox = createSandbox();
        beforeEach(async () => {
            logger = await emptyLogger;
        });
        afterEach(() => sandbox.restore);
    });  // end handle
}); // end service
