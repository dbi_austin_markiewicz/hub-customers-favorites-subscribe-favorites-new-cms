import { Context, SQSEvent, SNSMessage } from 'aws-lambda';
import { v4 as uuid } from 'uuid';
import { getEnvironmentVariable } from '@dbidss/dss-apis-authentication';
import { DssResponse, DssResponseCode, RepoFactory, initLogger, ServiceStore, toProperError } from '@dbidss/dss-apis-datastore';
import { LambdaInstance, Logger, LogFactory } from '@dbidss/dss-apis-logger';
import { DssError } from '@dbidss/dss-apis-validation';
import { SQSEventRecord, ChangeEvent, parseSqsRecord, parseArn, Customer, Event } from '@dbidss/dss-apis-models';

export interface HubTokenResponse { token: string; }

const lambdaInstance: LambdaInstance = { isColdStart: true, instanceId: uuid(), coldStartRequestId: '' };
const loggingOptions = { handler: 'hub-customers-favorites-subscribe-favorites-new-cms' };

const today: Date = new Date();

// REMOVE AFTER SUCCESSFULL FAVORITES PACKAGE
// const customerUuid = '40584f92-df5d-4877-8209-f7b453f117e1';

// process.env.DSSAPI_CUSTOMERS_CUSTOMERUUID = 'https://8hpmajtmn5.execute-api.us-east-1.amazonaws.com/dev/customers/';
// process.env.DSSAPI_CUSTOMERS_EVENT_EVENTID = 'https://8hpmajtmn5.execute-api.us-east-1.amazonaws.com/dev/events/';


export const parseIncomingEvent = <N, O>(logger: Logger, sqsEvent: SQSEventRecord, loggingOptions?: any): ChangeEvent<N, O> => {
    const changeEvent = {} as ChangeEvent<N, O>;
    let snsEvent: SNSMessage;

    try {
        logger.log.debug('parsing SNS event out of SQS event...', { ...loggingOptions });
        snsEvent = JSON.parse(sqsEvent.body);
        changeEvent.topicArn = parseArn(logger, snsEvent.TopicArn);
        changeEvent.dateTimeCreated = snsEvent.Timestamp;
    } catch (err) {
        const message = 'error while parsing SNS event out of SQS event';
        const error = toProperError(err);
        logger.log.error(message, { ...loggingOptions, sqsEvent, error });
        throw new DssError({ message });
    }
    let dataStream;
    try {
        logger.log.debug('parsing DDBDataStream event out of SNS event...', { ...loggingOptions, message: snsEvent.Message });
        dataStream = JSON.parse(snsEvent.Message);
    } catch (err) {
        const message = 'error while parsing DDBStream event out of SNS event';
        const error = toProperError(err);
        logger.log.error(message, { ...loggingOptions, snsEvent, error });
        throw new DssError({ message });
    }
    let newImage = {} as N;
    try {
        logger.log.debug('parsing newImage from ddbDataStream event', { ...loggingOptions, newImage: dataStream.NewImage });
        if (!!dataStream.newImage) {
            if (typeof dataStream.newImage !== 'object') {
                newImage = JSON.parse(dataStream.newImage);
            } else {
                newImage = dataStream.newImage;
            }
        }
        changeEvent.newImage = newImage;
    } catch (err) {
        const message = 'error while parsing newImage out of ddbDataStream event';
        const error = toProperError(err);
        logger.log.error(message, { ...loggingOptions, dataStream, error });
        throw new DssError({ message });
    }
    let oldImage = {} as O;
    try {
        logger.log.debug('parsing oldImage from ddbDataStream event', { ...loggingOptions });
        if (!!dataStream.oldImage) {
            if (typeof dataStream.oldImage !== 'object') {
                oldImage = JSON.parse(dataStream.oldImage);
            } else {
                oldImage = dataStream.oldImage;
            }
        }
        changeEvent.oldImage = oldImage;
    } catch (err) {
        const message = 'error while parsing oldImage out of ddbDataStream event';
        const error = toProperError(err);
        logger.log.error(message, { ...loggingOptions, dataStream, error });
        throw new DssError({ message });
    }

    return changeEvent;
};

/****************************************************** Auth Token *********************************************************/

// export const getAuthToken = 




/****************************************************** Service calls ******************************************************/
//export const handle = async ({ logger, data, repo }: { logger: Logger, data: SQSEventRecord, repo: RepoFactory }): Promise<any> => {
export const handle = async ({ logger, data, repo }: { logger: Logger, data: any, repo: RepoFactory }): Promise<any> => {
    try {
        //const incomingFav = parseIncomingEvent<any, any>(logger, data);
        const incomingFav = data;
        logger.log.info('incoming favorite', { ...loggingOptions, incomingFav });
        
        console.log('-------------------------------------');
        console.log('incoming favorite');
        console.log(incomingFav);
        
        const customerUuid = incomingFav.dynamodb.Keys.customerUuid.S;
        const authToken = incomingFav.dynamodb.Keys.authToken.S;
        
        // Get the eventIds for the user
        let events = await getEvents(logger, customerUuid, authToken, repo);
        
        console.log('-------------------------------------');
        console.log('getEvents');
        console.log(JSON.stringify(events));
        
        // Get the event info of the event this favorite should go to
        let eventInfo = await getEvent(logger, events, authToken, repo);
        
        console.log('-------------------------------------');
        console.log('using this event');
        console.log(eventInfo);
        
        // Run the sql to add the favorite to CMS
        
        
        // return Promise.resolve(DssResponse.from(eventResponse, DssResponseCode.OK));
        

    } catch (err) {
        const error = toProperError(err);
        logger.log.error('Error parsing incoming newImage', { ...loggingOptions, function: 'handle', error, alert: true });
        throw err;
    }
};



export const getEvents = async function (logger: Logger, customerUuid: string, authToken: string, repo: RepoFactory): Promise<Customer.Event[]> {
    
    try {
        let eventResponse = <ServiceStore.Response<any>>(await ((<ServiceStore<any>>repo.eventIdGET).getData({
            logger: logger,
            headers: {
                authorizationToken: authToken,
                'Content-Type': 'application/json'
            },
            path: `${customerUuid}/event`
        })));
        
        if (!!eventResponse && !!eventResponse.body) {
            return Promise.resolve(eventResponse.body);
        } else {
            return [];
        }
    } catch (err) {
        const message = 'Get customer events error';
        const error = toProperError(err);
        logger.log.debug(message, {...loggingOptions, error, alert: true, step: 'output'});
        throw new DssError({message});
    }
};



export const getEvent = async function (logger: Logger, events: Customer.Event[], authToken: string, repo: RepoFactory) { //: Promise<Event.Event>
    
    let eventsInfo: Event.Event[] = [];
    
    try {
        logger.log.debug('getting info for user events', {...loggingOptions, events: events});
        
        for (let x=0; x<events.length; x++) {
            let res = <ServiceStore.Response<any>>(await ((<ServiceStore<any>>repo.cmsEventIdGET).getData({
                logger: logger,
                headers: {
                    authorizationToken: authToken,
                    'Content-Type': 'application/json'
                },
                path: `${events[x].eventId}`
            })));
            
            const ev: Event.Event = res.body;
            
            // Verify the event before it's a candidate
                // eventTypeId == 1
                // eventDate > today
                // has cmsEventId
            if (!!ev && (ev.eventTypeId == 1) && (new Date(ev.eventDate) > today) && !!ev.cmsEventId) {
                eventsInfo.push(res.body);
            }
        };
        
        if (eventsInfo.length == 1) {
            // Only one event came back
            
            logger.log.debug('event identified', {...loggingOptions, event: eventsInfo[0]});
            
            return eventsInfo[0];
        } else {
            logger.log.info('Multiple future events with roleType 1, eventDate > today, and has cmsEventId', {...loggingOptions, alert: true, customerUuid: events[0].customerUuid, eventInfo: eventsInfo});
            
            // Take the nearest upcoming event
            let earliestEvent: Event.Event = eventsInfo[0];
            eventsInfo.forEach((ev) => {
                if (!!earliestEvent && (new Date(earliestEvent.eventDate) > (new Date(ev.eventDate)))) {
                    earliestEvent = ev;
                }
            });
            
            logger.log.debug('event identified', {...loggingOptions, event: earliestEvent});
            
            return earliestEvent;
        }
        
    } catch (err) {
        const message = 'Get events info error';
        const error = toProperError(err);
        logger.log.debug(message, {...loggingOptions, error, alert: true, step: 'output'});
        throw new DssError({message});
    }
    
};



/// logger.log.info('Multiple future events with roleType 1 ', {...loggingOptions, alert: true});



/* istanbul ignore next */
exports.lambdaHandler = async (event: SQSEvent, context: Context): Promise<DssResponse<any>> => {
    
    const start = new Date().getTime();
    const responses = [] as any[];
    const sqsBatchSize = !!event && !!event.Records ? event.Records.length : 0;

    console.log(JSON.stringify({
        message: 'incoming event to handler',
        level: 'DEBUG',
        ...loggingOptions,
        event,
        function: 'lambdaHandler'
    }));

    for (let i = 0; i < (<SQSEvent>event).Records.length; i++) {
        const logger = await initLogger(<any>event, context, lambdaInstance);
        logger.log.debug('', { ...loggingOptions, step: 'service', stage: 'START', event });
        
        
        const repoFactoryOptions: any = {
            eventIdGET: {
                type: ServiceStore.ServiceType.json,
                endpoint: await getEnvironmentVariable('DSSAPI_CUSTOMERS_CUSTOMERUUID'),
                method: ServiceStore.HttpMethod.GET
            },
            cmsEventIdGET: {
                type: ServiceStore.ServiceType.json,
                endpoint: await getEnvironmentVariable('DSSAPI_CUSTOMERS_EVENT_EVENTID'),
                method: ServiceStore.HttpMethod.GET
            }
        };
        
        const repo = {
            eventIdGET: await ServiceStore.init<any>({ logger, options: repoFactoryOptions.eventIdGET }),
            cmsEventIdGET: await ServiceStore.init<any>({ logger, options: repoFactoryOptions.cmsEventIdGET })
        };

        logger.log.info('event being processed...', { ...loggingOptions, index: i, sqsBatchSize, eventRecord: event.Records[i], function: 'handle' });
        try {
            const data = parseSqsRecord<any>(logger, (<SQSEvent>event).Records[i]);
            
            const response = await handle({ logger, data, repo });
            
            logger.log.info('duration log', { ...loggingOptions, step: 'service', stage: 'END', duration: { duration: new Date().getTime() - start, unit: 'ms' } });
            logger.log.info('', { ...loggingOptions, response });
            responses.push(response);
        } catch (err) {
            const message = 'Error processing new return';
            const error = toProperError(err);
            logger.log.error(message, { ...loggingOptions, function: 'lambdaHandler', error });
            throw new DssError({ message });
        }
    }
    return Promise.resolve(DssResponse.from(responses, DssResponseCode.OK));
};
