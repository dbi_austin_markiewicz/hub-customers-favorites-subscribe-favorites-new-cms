# hub-customers-favorites-subscribe-favorites-new-cms

This project contains the cloud formation templates for queue and lambda to deploy a hub subscriber.

vsCode is the recommended IDE

\*\* To duplicate into a new stack:

1. clone / copy root folder to new folder name
2. do global rename of hub-<hubname> to desired hub name
3. do global rename of Hub<HubName> to desired hub name
4. do global rename of HUB\_<HubName> to desired hub name
5. rename the files in lib to the appropriate hub name
6. create a new git repository in bitbucket and copy url
7. execute sudo rm -R .git
8. execute git init
9. execute git add --all && git commit -am 'initial commit'
10. execute git remote add origin <git url>
11. change branch name as appropriate (rename master to release/v1 typically for hubs)
    git branch -m <new name>
12. execute git push origin <branch name>
13. Do a global search for "TODO" and update as appropriate to add any service-specific logic needed

\*\* DEBUGGING

1. .vscode folder contains all debugging configurations and test code. This code will need to be updated for payloads
2. debugging uses lambda-local to run the lambda handler code. This is best for debugging business logic and lambda code only.
3. for Debugging template code, push to AWS dev and test from there (see scripts below)

\*\* MOST COMMONLY USED SCRIPTS
`npm run test`
-- runs tests locally
`npm run build`
-- builds code locally
`npm run build:debug`
-- builds code with sourceMaps (required for debugging locally)
`npm run release`
-- builds, webpacks and compresses to zip
`npm run aws:deploy`
-- deploys cloud formation template to DEV account with dev parameters file
`npm run aws:all`
-- builds, webpacks, zips code, deploys template and pushes all code to aws (best for testing/debugging)

\*\* BUILD PIPELINE
-- to add a build pipeline for this repository to Jenkins:

1.  go to DataHub Jenkins page,
2.  click on newItem
3.  duplicate a similar hub repo TRIGGER
4.  then push to release/v1 branch
