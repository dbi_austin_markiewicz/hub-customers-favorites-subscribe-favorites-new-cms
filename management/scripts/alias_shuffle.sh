#!/bin/bash

ENV=$1
stack_name=$2
ENV_ROLE=$3
logging_level=${4-info} #default to info if not present

if [ $ENV_ROLE ];
then
    echo "assuming $ENV_ROLE role"
    export AWS_DEFAULT_REGION="us-east-1"
    $(aws sts assume-role --role-arn $ENV_ROLE \
        --role-session-name "jenkins-$ENVAccount"  | jq -r  \
        '.Credentials | "export AWS_SESSION_TOKEN=\(.SessionToken)\nexport AWS_ACCESS_KEY_ID=\(.AccessKeyId)\nexport AWS_SECRET_ACCESS_KEY=\(.SecretAccessKey) "')
fi;

s3_bucket="hub-lambdas-$ENV"
s3_folder="lambdas"

# get list of non-splunk lambda functions from stack
stack_resources=$(aws cloudformation list-stack-resources --stack-name "$stack_name")

lambda_functions=$(echo "$stack_resources" | jq  ".StackResourceSummaries[] | select(.ResourceType == \"AWS::Lambda::Function\") | select(.PhysicalResourceId | contains(\"splunk\") != true) | select(.PhysicalResourceId | contains(\"auth\") != true) | .PhysicalResourceId" | sed s/\"//g)

package_version=$(cat package.json | jq ".version" | sed s/\"//g)
deployable_version=$(echo $package_version | cut -d'.' -f1)
deploy_version=$(echo v$deployable_version)

echo "package version $package_version found - deploying version $deploy_version"

echo "shuffling lambda functions"
for function in $lambda_functions; do
    echo "------------------------------------------------------"
    echo $function
    echo "------------------------------------------------------"
    
    function_aliases=$(aws lambda list-aliases --function-name "$function")
    # previous
    temp_version=$(echo "$function_aliases" | jq ".Aliases[] | select(.Name == \"temp\") | .FunctionVersion" | sed s/\"//g)
    printf "temp:\t\t$temp_version\n"

    # version version
    version_version=$(echo "$function_aliases" | jq ".Aliases[] | select(.Name == \"$deploy_version\") | .FunctionVersion" | sed s/\"//g)
    printf "version $deploy_version:\t$version_version\n"

    echo "------------------------------------------------------"

    if [ $temp_version ];
    then
        if [ $version_version ];
        then
            echo "  updating $deploy_version alias from version $version_version -> $temp_version"
            aws lambda update-alias --function-name $function --name $deploy_version --function-version $temp_version
        else
            echo "  creating $deploy_version alias from version $temp_version"
            aws lambda create-alias --function-name $function --name $deploy_version --function-version $temp_version
        fi
    else
        echo "  no current version currently exists - doing nothing"
    fi

    echo "------------------------------------------------------"
    printf "\n\n\n\n"
done

echo "shuffling api gateway stages"
api_gateways=$(echo "$stack_resources" | jq  ".StackResourceSummaries[] | select(.ResourceType == \"AWS::ApiGateway::RestApi\")| .PhysicalResourceId" | sed s/\"//g)
for gateway in $api_gateways; do
    echo "------------------------------------------------------"
    echo "Shuffling api gateway stages for api $gateway"
    echo "------------------------------------------------------"
    gateway_stages=$(aws apigateway get-stages --rest-api-id $gateway)

    # temp
    temp_stage=$(echo "$gateway_stages" | jq ".item[] | select(.stageName == \"temp\") | .deploymentId" | sed s/\"//g)
    printf "temp:\t$temp_stage\n"

    # version version
    version_stage=$(echo "$gateway_stages" | jq ".item[] | select(.stageName == \"$deploy_version\") | .deploymentId" | sed s/\"//g)
    printf "version $deploy_version:\t$version_stage\n"

    echo "------------------------------------------------------"
    echo "updating previous stage"
    if [ $temp_stage ];
    then
        if [ $version_stage ];
        then
            echo "  updating $deploy_version stage from deployment $temp_stage"
            aws apigateway update-stage --rest-api-id $gateway --stage-name $deploy_version --patch-operations "op=replace,path=/deploymentId,value=$temp_stage"
        else
            echo "  creating $deploy_version stage from deployment $temp_stage"
            aws apigateway create-stage --rest-api-id $gateway --stage-name $deploy_version --deployment-id $temp_stage --variables lambdaAlias=$deploy_version,loggingLevel=$logging_level
        fi
    else
        echo "  no current stage current exists - doing nothing"
    fi

    echo "------------------------------------------------------"
done