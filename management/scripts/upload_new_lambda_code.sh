#!/bin/bash

ENV=$1
stack_name=$2
ENV_ROLE=$3

if [ $ENV_ROLE ];
then
    echo "assuming $ENV_ROLE role"
    export AWS_DEFAULT_REGION="us-east-1"
    $(aws sts assume-role --role-arn $ENV_ROLE \
        --role-session-name "jenkins-$ENVAccount"  | jq -r  \
        '.Credentials | "export AWS_SESSION_TOKEN=\(.SessionToken)\nexport AWS_ACCESS_KEY_ID=\(.AccessKeyId)\nexport AWS_SECRET_ACCESS_KEY=\(.SecretAccessKey) "')
fi;

s3_bucket="hub-lambdas-$ENV"
s3_folder="lambdas"


stack_resources=$(aws cloudformation list-stack-resources --stack-name "$stack_name")
functions=$(echo "$stack_resources" | jq  ".StackResourceSummaries[] | select(.ResourceType == \"AWS::Lambda::Function\") | select(.PhysicalResourceId | contains(\"splunk\") != true) | .PhysicalResourceId" | sed s/\"//g)

functions=$(
    for i in $functions; do
        echo "$i.zip"
    done
)

local_zips=$(ls "release")

echo $functions;

for zip in $local_zips; do
    echo "------------------------------------------------------"
    echo $zip
    echo "------------------------------------------------------"
    
    if [[ ${functions[*]} =~ $zip ]];
    then
        # match found - do nothing
        echo "matching zip for $zip found - doing nothing"
    else
        # no match found - upload to s3
        echo "matching zip for $zip not found in s3 bucket - uploading"
        aws s3 cp "release/$zip" "s3://$s3_bucket/$s3_folder/$zip"
    fi
    echo "------------------------------------------------------"
    printf "\n"
done