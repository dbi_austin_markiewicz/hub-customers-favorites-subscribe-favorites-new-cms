#!/bin/bash

ENV=$1
stack_name=$2
ENV_ROLE=$3

if [ $ENV_ROLE ];
then
    echo "assuming $ENV_ROLE role"
    export AWS_DEFAULT_REGION="us-east-1"
    $(aws sts assume-role --role-arn $ENV_ROLE \
        --role-session-name "jenkins-$ENVAccount"  | jq -r  \
        '.Credentials | "export AWS_SESSION_TOKEN=\(.SessionToken)\nexport AWS_ACCESS_KEY_ID=\(.AccessKeyId)\nexport AWS_SECRET_ACCESS_KEY=\(.SecretAccessKey) "')
fi;

s3_bucket="hub-lambdas-$ENV"
s3_folder="lambdas"

# get list of non-splunk lambda functions from stack
stack_resources=$(aws cloudformation list-stack-resources --stack-name "$stack_name")
functions=$(echo "$stack_resources" | jq  ".StackResourceSummaries[] | select(.ResourceType == \"AWS::Lambda::Function\") | select(.PhysicalResourceId | contains(\"splunk\") != true) | .PhysicalResourceId" | sed s/\"//g)

echo $functions

temp_directory="temp"
packed_directory="packed"
release_directory="release"

package_version=$(cat package.json | jq ".version" | sed s/\"//g)
deployable_version=$(echo $package_version | cut -d'.' -f1)
deploy_version=$(echo v$deployable_version)

echo "package version $package_version found - deploying version $deploy_version"

# create required temp directory    
# rm -r $temp_directory
mkdir $temp_directory

for function_name in $functions; do
    echo "------------------------------------------------------"
    echo $function_name

    echo "------------------------------------------------------"
    aws s3 cp "s3://$s3_bucket/$s3_folder/$function_name.zip" "temp"
    unzip "$temp_directory/$function_name.zip" -d "temp"
    echo "------------------------------------------------------"

    # get aliased version first
    lambda_function=$(aws lambda get-function --function-name $function_name --qualifier $deploy_version)
    if [ -z ${lambda_function} ];
    then
        lambda_function=$(aws lambda get-function --function-name $function_name)
    fi
    lambda_hash=$(echo "$lambda_function" | jq ".Configuration | .CodeSha256" | sed s/\"//g)
    printf "lambda function zip hash:\t$lambda_hash\n"

    s3_zip_hash=$(openssl dgst -sha256 -binary "$temp_directory/$function_name.zip" | openssl enc -base64)
    printf "s3 bucket zip hash:\t\t$s3_zip_hash\n"

    current_hash=$(openssl dgst -sha256 -binary "$temp_directory/$function_name.js" | openssl enc -base64)
    printf "s3 bucket js hash:\t\t$current_hash\n"

    new_hash=$(openssl dgst -sha256 -binary "$packed_directory/$function_name.js" | openssl enc -base64)
    printf "local js hash:\t\t\t$new_hash\n"

    echo "------------------------------------------------------"

    if [ ${lambda_hash} != ${s3_zip_hash} ];
    then
        echo "\033[33mWarning! The code in the lambda differs from its code in s3\033[0m"
    fi

    if [ ${current_hash} != ${new_hash} ];
    then
        echo "Hashes differ - uploading updated code zip"
        aws s3 cp "$release_directory/$function_name.zip" "s3://$s3_bucket/$s3_folder/$function_name.zip"
        s3_key="$s3_folder/$function_name.zip"
        if [[ $function_name == *"auth"* ]];
        then
            echo "$function_name updating lambda code without versioning"
            lambda_update_response=$(aws lambda update-function-code --function-name $function_name --s3-bucket $s3_bucket --s3-key $s3_key)
        else
            echo "Publishing new version of lambda"
            lambda_update_response=$(aws lambda update-function-code --function-name $function_name --s3-bucket $s3_bucket --s3-key $s3_key --publish)
            # parse new lambda version out of arn
            new_lambda_version=$(echo "$lambda_update_response" | jq ".Version" | sed s/\"//g)
            echo "Creating/Updating $deploy_version alias to point to new lambda version: $new_lambda_version"
            alias_update_response=$(aws lambda update-alias --function-name $function_name --function-version $new_lambda_version --name "temp")
            fi
    else
        echo "No difference in hashes - doing nothing"
    fi
    echo "------------------------------------------------------"
    printf "\n\n"
done
rm -r $temp_directory