#!/bin/bash

ENV=$1
stack_name=$2
ENV_ROLE=$3
echo "Environment: $ENV"

if [ $ENV_ROLE ];
then
    echo "assuming $ENV_ROLE role"
    export AWS_DEFAULT_REGION="us-east-1"
    $(aws sts assume-role --role-arn $ENV_ROLE \
        --role-session-name "jenkins-$ENVAccount"  | jq -r  \
        '.Credentials | "export AWS_SESSION_TOKEN=\(.SessionToken)\nexport AWS_ACCESS_KEY_ID=\(.AccessKeyId)\nexport AWS_SECRET_ACCESS_KEY=\(.SecretAccessKey) "')
fi;

template_file=management/template.yaml
parameters=management/parameters/parameters.$ENV.json
echo "Stackname: $stack_name"
echo "Template file: $template_file"
echo "Parameters: $parameters"

package_version=$(cat package.json | jq ".version" | sed s/\"//g)
deployable_version=$(echo $package_version | cut -d'.' -f1)
deploy_version=$(echo v$deployable_version)

aws cloudformation deploy --stack-name $stack_name --template-file $template_file --parameter-overrides $(jq -r '.[] | [.ParameterKey, .ParameterValue] | join("=")' $parameters) --no-fail-on-empty-changeset --capabilities "CAPABILITY_NAMED_IAM" "CAPABILITY_AUTO_EXPAND"
