packed_files=$(ls packed)
echo $packed_files
mkdir "release"
for i in $packed_files; do
    zip_name=$(echo $i | sed -e "s/.js//g")
    echo $zip_name
    ( cd packed && zip -X -D "../release/$zip_name.zip" "$i" )
done