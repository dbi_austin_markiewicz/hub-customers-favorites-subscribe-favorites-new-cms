#!/bin/bash

ENV_ROLE=$1

# assume role
if [ $ENV_ROLE ];
then
    echo "assuming $ENV_ROLE role"
    export AWS_DEFAULT_REGION="us-east-1"
    $(aws sts assume-role --role-arn $ENV_ROLE \
        --role-session-name "jenkins-$ENVAccount"  | jq -r  \
        '.Credentials | "export AWS_SESSION_TOKEN=\(.SessionToken)\nexport AWS_ACCESS_KEY_ID=\(.AccessKeyId)\nexport AWS_SECRET_ACCESS_KEY=\(.SecretAccessKey) "')
fi;

# upload template
aws s3 cp "management/template.yaml" "s3://dss-cloudformation-tests/templates/template.yaml"

# validate yaml template
aws cloudformation validate-template --template-url "s3://dss-cloudformation-tests/templates/template.yaml"

# delete yaml template from temp s3 bucket
aws s3 rm "s3://dss-cloudformation-tests/templates/template.yaml"